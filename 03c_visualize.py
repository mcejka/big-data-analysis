import pymongo
import hdbscan
import numba
import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from bertopic import BERTopic
from sentence_transformers import SentenceTransformer
from umap import UMAP

experiment_name     = 'csr_2023-02-23_all_docs'
root_directory      = 'C:/Users/uzivatel/big-data-analysis/'
model_directory     = os.path.join(root_directory, 'model')
result_directory    = os.path.join(root_directory, 'result')
experiment_model    = os.path.join(model_directory, experiment_name)
experiment_result   = os.path.join(result_directory, experiment_name)

#load processed data from database
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["big_data_analysis"]
df = pd.DataFrame(list(mydb.english_col.find({})))

df['clean_english'] = df['clean_english'].replace(r'', np.nan, regex=True)
df['clean_english'] = df['clean_english'].replace(r'NaN', np.nan, regex=True)
df                  = df.dropna(subset=['clean_english'])
df['clean_english'] = df['clean_english'].astype(str)
docs = df['clean_english'].to_list()
#docs = df['clean_english'][1:10000].to_list()

topic_model = BERTopic.load(os.path.join(experiment_model, 'csr_bertopic_default'))

f = open(os.path.join(experiment_result, 'topic_info'), "a")
f.write(str(topic_model.get_topic_info()))
f.write('\n')
f.write(str(topic_model.get_document_info(docs)))
f.write('\n')

fig_1 = topic_model.visualize_topics()
fig_1.write_html(os.path.join(result_directory, 'topic_info'))

min_model = topic_model.reduce_topics(docs, nr_topics=10)
min_model.save(os.path.join(experiment_model, 'csr_bertopic_min_model'))

fig_2 = min_model.visualize_topics()
fig_2.write_html(os.path.join(result_directory, 'topic_info_min_model'))

states = df['country_code'].to_list()

topics_per_class = min_model.topics_per_class(docs, states)
fig_3 = min_model.visualize_topics_per_class(topics_per_class)
fig_3.write_html(os.path.join(result_directory, 'topics_per_class'))

f.write(str(min_model.get_document_info(docs)))
f.write('\n')

topic_labels = min_model.generate_topic_labels(nr_words=8, separator=", ")
f.write(str(topic_labels))
f.write('\n')

frequency = min_model.get_topic_freq()
f.write(str(frequency))
f.write('\n')

f.close()