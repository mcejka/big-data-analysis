import os
import numpy as np

experiment_name     = 'testPath'
root_directory      = 'C:/Users/uzivatel/big-data-analysis/'
model_directory     = os.path.join(root_directory, 'model')
result_directory    = os.path.join(root_directory, 'result')
experiment_model    = os.path.join(model_directory, experiment_name)
experiment_result   = os.path.join(result_directory, experiment_name)

os.mkdir(experiment_model)
os.mkdir(experiment_result)

x = np.arange(15, dtype=np.int64).reshape(3, 5)

np.save(os.path.join(experiment_model, 'csr_all-mpnet-base-v2_encoded'), x)