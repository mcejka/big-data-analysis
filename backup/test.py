import numpy as np
from umap import UMAP

try:
    embeddings = np.load('./model/csr_all-mpnet-base-v2_encoded.npy')
except:
    print("Error in loading embeddings")

#reduce dimensions to improve 
try:
    umap_embeddings = UMAP(n_neighbors=15,     
                            n_components=5, 
                            metric='cosine',
                            low_memory=True).fit_transform(embeddings)
except:
    print("Error in reducing dimensions")

print(type(umap_embeddings))
np.save('./model/csr_all-mpnet-base-v2_umap', umap_embeddings)