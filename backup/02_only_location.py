import pymongo
import pandas as pd
import numpy as np

#load dataset from mongo db
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["big_data_analysis"]
df = pd.DataFrame(list(mydb.mycol.find({})))
before = df.shape[0]

#remove empty records and records with no user_location specified
df = df.dropna(subset=['user_location<gx:text>','text<gx:text>']).reset_index(drop=True)
df['user_location<gx:text>'] = df['user_location<gx:text>'].astype(str)
df['text<gx:text>'] = df['text<gx:text>'].astype(str)
after = df.shape[0]

print(before, after, before - after)

#create country code column with empty values
df['country_code'] = np.nan

#load database collection with country codes
df_cc = pd.DataFrame(list(mydb.country_code_col.find({})))

for i in range (df_cc.shape[0]):
    row = df_cc['row'][i]
    df['country_code'][row-1] = df_cc['country_code'][i]

#remove lines with error or empty country_code
df['country_code'] = df['country_code'].replace(r'NaN', np.nan, regex=True)
df = df.dropna(subset=['country_code']).reset_index(drop=True)
after_cc = df.shape[0]

print(after, after_cc, after - after_cc)

#drop unwanted columns
df = df.drop(columns=['_id','author_id<gx:category>','author_name<gx:category>', 'author_handler<gx:category>', 'author_avatar<gx:url>', 
                   'user_created_at<gx:date>', 'user_description<gx:text>', 'user_favourites_count<gx:number>', 'user_followers_count<gx:number>',
                   'user_following_count<gx:number>', 'user_listed_count<gx:number>', 'user_tweets_count<gx:number>', 'user_verified<gx:boolean>',
                   'user_location<gx:text>', 'lang<gx:category>', 'type<gx:category>', 'mention_ids<gx:list[category]>', 'mention_names<gx:list[category]>',
                   'retweets<gx:number>','favorites<gx:number>','replies<gx:number>','quotes<gx:number>','links<gx:list[url]>', 'links_first<gx:url>',
                   'image_links<gx:list[url]>', 'image_links_first<gx:url>','rp_user_id<gx:category>', 'rp_user_name<gx:category>', 'location<gx:text>',
                   'tweet_link<gx:url>','source<gx:text>', 'search<gx:category>'])

df['country_code'] = df['country_code'].astype(str)

df.to_csv(r'./data/csr_02_only_location.csv', index=False)

df.iloc[0:0]