import pymongo
import pandas as pd
import numpy as np
from bertopic import BERTopic
from sentence_transformers import SentenceTransformer
from umap import UMAP

topic_model = BERTopic.load("./model/all-mpnet-base-v2_min_csr")


