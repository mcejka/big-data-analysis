import re
import pandas as pd
import numpy as np


#import data from csv
df = pd.read_csv('./data/csr_02_only_location.csv')
before = df.shape[0]

#pre-process data (language independend)
def cleanBasics(x):
  # convert to string
  x = str(x)
  # convert to lower case
  x = x.lower()
  # remove hashtag words
  x = re.sub(r'#[A-Za-z0-9]*', ' ', x)
  # remove links
  x = re.sub(r'https*://.*', ' ', x)
  # remove user mentions
  x = re.sub(r'@[A-Za-z0-9]+', ' ', x)
  # remove special characters
  x = re.sub(r'[%s]' % re.escape('!"#$%&\()*+,-./:;<=>?@[\\]^_`{|}~“…”’'), ' ', x)
  # remove numbers
  x = re.sub(r'\d+', ' ', x)
  # remove newlines
  x = re.sub(r'\n+', ' ', x)
  # remove mulitple spaces
  x = re.sub(r'\s{2,}', ' ', x)
  return x
df['clean_basic'] = df['text<gx:text>'].apply(cleanBasics)

#remove lines with error or empty country_code
df['clean_basic'] = df['clean_basic'].replace(r'', np.nan, regex=True)
df['clean_basic'] = df['clean_basic'].replace(r' ', np.nan, regex=True)
df['clean_basic'] = df['clean_basic'].replace(r'NaN', np.nan, regex=True)
df = df.dropna(subset=['clean_basic']).reset_index(drop=True)
after = df.shape[0]

print(before, after, before - after)

df.to_csv(r'./data/csr_03_clean_basic.csv', index=False)

df.iloc[0:0]