import numpy as np
import os

from umap import UMAP

os.environ['KMP_DUPLICATE_LIB_OK']='True'

embeddings = np.load('./model/csr_all-mpnet-base-v2_encoded.npy')

umap_embeddings = UMAP(n_neighbors=15,     
                        n_components=5, 
                        metric='cosine',
                        low_memory=True,
                        verbose=True).fit_transform(embeddings)

np.save('./model/csr_all-mpnet-base-v2_umap_he', umap_embeddings)