# big-data-analysis

## Zprovoznění kódu

- Instalace git
- Instalace SDK (například VS Code)
- Instalace Pluginů do SDK (viz struktura projektu níže)
- Instalace Pythonu (viz struktura projektu níže)
- Spárování interpreteru Pythonu s SDK
- Klonování repozitáře (git clone "URL repozitáře")

## Způsob implementace změn

- Provést změny
- Uložit soubory se změnami
- Commit se zprávou (git commit -m "co se změnilo")
- Push na Gitlab (git push -uf origin main)

## Struktura projektu

Projekt je vyvíjen v následujícím prostředí:

- laboratoř umělé inteligence PEF ČZU, počítač matlab-pc-01
- operační systém: Windows 10 Education
- procesor: AMD EPYC 7601 (32 jader, 2.2GHz), 2 procesory
- nainstalovaná paměť: 1,5 TB
- GPU: NVIDIA Quadro P6000, CUDA: 11.3.0, CUDNN: 8.7.0.84
- SDK: Visual Code
- Extensions ve VS Code: Dev Containers, Excel Viewer, isort, Jupyter, Jupyter Cell Tags, Jupyter Keymap, Jupyter Notebook Renderers, Jupyter Slide Show, Pylance, Python, Remote - SSH, Remote - SSH: Editing Configuration Files, Remote Development, Remote Explorer, VS Code Jupyter Notebook Previewer, WSL, Docker, markdownlint, Git History, Git Project Manager
- Python: virtuální environment nástroje Anaconda, conda 22.9.0, Python 3.10.4, pip 22.2.2
- Databáze: lokální instance open-source Mongo DB s nástrojem MongoDB Compass

### 01_data_transformer.ipynb

Jupyter Notebook s účelem převést dataset do dokumentové databáze Mongo DB, která je navržena pro tzv. Big Data.
Mongo DB je pro tyto účely nainstalována lokálně v laboratoři umělé inteligence.

- import knihovny PyMongo pro práci s Mongo DB
- import knihovny Pandas pro práci s datasetem
- import knihovny json pro převedení záznamů v datasetu do objektů
- načtení datasetu a validace načtení vypsáním počtem zázanamů a jejich atributů
- vytvoření instance Mongo DB, kolekce
- transformace hlaviček záznamů datasetu (názvů atributů) do seznamu
- transformace záznamů datasetu do objektů
- vložení objektů do vytvořené instance dokumentové databáze Mongo DB
- validace transformace

### 02_data_processing.ipynb

Jupyter Notebook s účelem vyčistit a předzpracovat dataset pro modelování a vizualizaci.

- import knihovny Pandas pro práci s datasetem
- import knihovny requests pro API volání Google Maps
- import knihovny country-converter pro získání ISO-2 kódu země (např. CZ)
- import knihovny re pro práci s regulárními výrazy
- načtení datasetu, validace pomocí vypsání počtu záznamů a počtu jejich atributů
- odstranění záznamů, jejichž atributy text a user_location jsou prázdné. Resetování indexu.
- definice funkce get_country_code, která přijme argument (řetězec). Pokud není prázdný, je sestavena url obsahující přijmutý řetězec. Je poslána API žádost na Google Maps Platform s tímto řetězcem a příjmána odpověď - lokace. Je-li v odpovědi alespoň jeden kandidát, je z něj extrahována země. Pro tuto zemi se volá knihovna country_code, která vrací kód země ve formátu ISO2. V případě, že kód země má opravdu dva znaky, funkce vrátí odpověď - kód země. V každém jiném případě vrací hodnotu 'Nan'.
- funkce get_country_code je volána pro každou hodnotu sloupce user_location nahraného datasetu a nahrána do nového sloupce country_code
- odstranění záznamů, kde není countru_code nebo není validní
- přidání sloupce s délkou sloupce řetězců ve sloupci "text"
- extrakce entit - označených uživatelů, a také pozic v textu, kde jsou označeni, a to do dvou nových sloupců
- čištění textu nezávislé na jazyku (převod do řetězce, převod na lower-case, odstranění hashtagů, odstranění odkazů, odstranění zmíněných uživatelů, odstranění speciálních znaků, čísel, odřádkování a několikanásobných mezer)
- detekce jazyka pro očištěný text jako nový sloupec
- překlad do angličtiny pomocí nástrojů deepL a google transalator do nového sloupce "translated"
- čištění anglického přeloženého text (odstranění po sobě jdoucích duplikovaných slov, odstranění stopwords)
- uložení datasetu do nového souboru jako předzpracovaný dataset

### 03_data_modelling.ipynb

Jupyter notebook s účelem trénování různých modelů, následné clusterování a vizualizace

- načtení datasetu
- konverze textu k analýze do pole - zvlášť s očištěným textem v angličtině a s multi-jazyčným textem
- načtení předtrénovaných modelů s all-MiniLM-L6-v2, all-mpnet-base-v2, paraphrase-xlm-r-multilingual-v1 a paraphrase-distilroberta-base-v1
- vytvoření korpusu, tedy zakódování pole textů do jednotlivých modelů
- trénink nástrojem umap - tzv. manifold learning, který redukuje dimenze
- definice clusterovacího modelu hdbscan
- dotrénování hdbscan za využití natrénovaného umap modelu
- využití dvou technik trénování pomocí nástroje BERTopic
- využití clusterovací metody KMeans

Vizualizace

- vizualizace délky tweetů (originální tweety před zpracováním)
- vizualizace clusterů v dvou dimennzách nástrojem hdbscan -