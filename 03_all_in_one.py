import pymongo
import hdbscan
import numba
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
import numba

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.datasets import fetch_20newsgroups
from bertopic import BERTopic
from sentence_transformers import SentenceTransformer
from umap import UMAP

experiment_name     = 'csr_2023-02-24_all_docs'
root_directory      = 'C:/Users/uzivatel/big-data-analysis/'
model_directory     = os.path.join(root_directory, 'model')
result_directory    = os.path.join(root_directory, 'result')
experiment_model    = os.path.join(model_directory, experiment_name)
experiment_result   = os.path.join(result_directory, experiment_name)

os.mkdir(experiment_model)
os.mkdir(experiment_result)

#load processed data from database
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["big_data_analysis"]
df = pd.DataFrame(list(mydb.english_col.find({})))

df['clean_english'] = df['clean_english'].replace(r'', np.nan, regex=True)
df['clean_english'] = df['clean_english'].replace(r'NaN', np.nan, regex=True)
df                  = df.dropna(subset=['clean_english'])
df['clean_english'] = df['clean_english'].astype(str)
docs = df['clean_english'].to_list()
#docs = df['clean_english'][1:10000].to_list()

sentence_model = SentenceTransformer("all-mpnet-base-v2")

embeddings = sentence_model.encode(docs, show_progress_bar=True)
np.save(os.path.join(experiment_model, 'csr_all-mpnet-base-v2_encoded'), embeddings)

umap_model = UMAP(n_neighbors=15,
                  n_components=10,
                  min_dist=0.0,
                  metric='euclidean',
                  low_memory=False)

topic_model = BERTopic(umap_model=umap_model)

topics, probs = topic_model.fit_transform(docs, embeddings)

topic_model.save(os.path.join(experiment_model, 'csr_bertopic_default'))

